package ru.t1.dzelenin.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
