package ru.t1.dzelenin.tm.command.user;

import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getDescription() {
        return "user remove";
    }

}
