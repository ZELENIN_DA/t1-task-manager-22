package ru.t1.dzelenin.tm.command.task;

import ru.t1.dzelenin.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskListCommand extends AbstractTaskCommand {

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

}
