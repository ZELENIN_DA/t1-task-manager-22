package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;
import ru.t1.dzelenin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAll(String userId, TaskSort sort);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeTaskStatusId(String userId, String id, Status status);

    Task changeTaskStatusIndex(String userId, Integer index, Status status);

}


