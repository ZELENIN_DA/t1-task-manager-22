package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M removeOneById(String id);

    M removeOneByIndex(Integer index);

    M removeOne(M model);

    void removeAll(Collection<M> collection);

    void removeAll();

    Integer getSize();

}


