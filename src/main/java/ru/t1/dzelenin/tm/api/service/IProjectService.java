package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;

public interface IProjectService extends IUserOwnerService<Project> {

    Project changeProjectStatusIndex(String userId, Integer index, Status status);

    Project changeProjectStatusId(String userId, String id, Status status);

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

}



