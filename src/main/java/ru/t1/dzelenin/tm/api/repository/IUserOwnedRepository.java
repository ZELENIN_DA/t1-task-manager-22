package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M removeOneById(String userId, String id);

    int getSize(String userId);

    M add(String userId, M model);

    M removeOneByIndex(String userId, Integer index);

    M removeOne(String userId, M model);

    void removeAll(String userId);

}




