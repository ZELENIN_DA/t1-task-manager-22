package ru.t1.dzelenin.tm.enumerated;

import ru.t1.dzelenin.tm.comporator.CreatedComparator;
import ru.t1.dzelenin.tm.comporator.NameComparator;
import ru.t1.dzelenin.tm.comporator.StatusComparator;
import ru.t1.dzelenin.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Without sort", null);

    private final String name;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    TaskSort(final String displayName, final Comparator<Task> comparator) {
        this.name = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return name;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
